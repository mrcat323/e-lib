<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
#use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     */
    public function test_book_creation_with_category(): void
    {
        $book = Book::factory()->create([
            'title' => 'Evgeniy Onegin'
        ]);

        $this->assertEquals('Evgeniy Onegin', $book->title);
    }

    public function test_book_update(): void
    {
        $book = Book::factory()->create([
            'updated_at' => null
        ]);

        $book->update([
            'title' => 'Evgeniy Onegin'
        ]);

        $this->assertNotNull($book->updated_at, 'it should NOT be null');
    }

    public function test_book_delete(): void
    {
        $book = Book::factory()->create();

        $book->delete();

        $books = Book::all();

        $this->assertEmpty($books);
    }

    public function test_book_list(): void
    {
        $books = Book::factory()->count(5)->create();

        $this->assertEquals(5, $books->count());
        // $this->assertCount(5, $books);
    }
}
