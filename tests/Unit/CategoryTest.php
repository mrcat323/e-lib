<?php

namespace Tests\Unit;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
// use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_category_creation(): void
    {
        $category = Category::factory()->create([
            'name' => 'Romance'
        ]);

        $this->assertEquals('Romance', $category->name);
    }

    public function test_category_update(): void
    {
        $category = Category::factory()->create([
            'updated_at' => null
        ]);

        $category->update([
            'name' => 'Drama'
        ]);

        $this->assertNotNull($category->updated_at, 'It should be NOT null');
    }

    public function test_category_delete(): void
    {
        $category = Category::factory()->create();

        $category->delete();

        $categories = Category::all();

        $this->assertEmpty($categories, 'It should BE empty');
    }

    public function test_category_list(): void
    {
        $categories = Category::factory()->count(5)->create();

        $this->assertEquals(5, $categories->count());
    }
}
